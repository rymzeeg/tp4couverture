package poste;

public class ColisExpressInvalide extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8132393212255562523L;

	public ColisExpressInvalide(String string) {
		super(string);
	}

}
