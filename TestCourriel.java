package courriel;


import static org.junit.Assert.assertThrows;

import java.util.*;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

public class TestCourriel {

		@Test
		public void testAdresse() throws Exception{
			Courriel c1=new Courriel("abc@yahoo.fr","help","blabla",new ArrayList<String>());
			c1.envoyer();
			
		}
		
		@Test
		public void testFausseAdresse() {
			Courriel c3=new Courriel("abc@yahoo","help","blabla",new ArrayList<String>(Arrays.asList("chemin")));
			
			assertThrows(Exception.class, () -> {c3.envoyer();});
		}
		
		@Test
		public void testBonCourrielAvecPJ() throws Exception {
			Courriel c1=new Courriel("abc@yahoo.fr","help","blabla avec PJ",new ArrayList<String>(Arrays.asList("chemin")));
		       c1.envoyer();
		}
		
		@Test
		public void testSansPJ() {
			Courriel c2=new Courriel("abc@yahoo.fr","titre","blabla PJ",new ArrayList<String>());
			
			assertThrows(Exception.class, ()-> {c2.envoyer();});
		}
		
		@Test
		public void testBonTitre() throws Exception{
			Courriel c1=new Courriel("abc@yahoo.fr","help","blabla",new ArrayList<String>());
			c1.envoyer();
		}


	@Test
	public void testBadTitle() {
		Courriel c2=new Courriel("abc@yahoo.fr","","blabla",new ArrayList<String>());
		
		assertThrows(Exception.class, ()-> {c2.envoyer();});
	}

	@ParameterizedTest
	@ValueSource(strings={"you@rien"})
	public void testAd(String address) {
		Courriel c=new Courriel(address,"titre","blabla",new ArrayList<String>());
		
		assertThrows(Exception.class, ()-> {c.envoyer();});
	}



	@ParameterizedTest
	@MethodSource("retourneMessage")
	//@CsvSource({"you@rien,pas de fin","@gmail.com,pas de début" })
	public void testDeTrop(String address,String raison) {
		  Courriel c=new Courriel(address,"titre","blabla",new ArrayList<String>());
		  
		  assertThrows(Exception.class, ()-> {c.envoyer();});
	}

	static Stream<Arguments> retourneMessage(){
		return Stream.of(
			Arguments.of("you@rien","pas de fin "),
			Arguments.of("@gmail.com","pas de début")
		);
	}
		
	}




