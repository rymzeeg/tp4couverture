package poste;
import java.util.ArrayList;
import java.util.*;
import java.lang.*;
import java.io.*;

public class Courriel {
// adresse électronique de destination, 
	//un titre de message, 
	//un corps de message et des pièces jointes. 
	//Les pièces jointes sont stockées sous forme de chemins vers des fichiers.
	
	private String adresse;
	private String titre;
	private String corpsMsg;
	private ArrayList<String> chemins;
	
	public Courriel(String adresse,String titre,String corpsMsg,ArrayList<String> chemins) {
		this.adresse=adresse;
		this.titre=titre;
		this.corpsMsg=corpsMsg;
		this.chemins=chemins;
	}
	
	
	//ACCESSEURS EN LECTURE
 public String getAdresse() {
	 return this.adresse;
 }
 
 public String getTitre() {
	 return this.titre;
 }
 
 public String getCorpsMsg() {
	 return this.corpsMsg;
 }
 
 
 
 //ACCESSEURS EN ECRITURE
 
 public void setAdresse(String a) {
	 this.adresse=a;
 }
 
 public void setTitre(String t) {
	 this.titre=t;
	 }
 
 
public void setCorpsMsg(String c){
	 this.corpsMsg=c;
}
 
public void envoyer() throws Exception{
	if(!adresse.matches("^[a-zA-Z][A-Za-z0-9]+@[a-zA-Z]+\\.[A-Za-z]+$")) { 
		throw new Exception("entre une adresse mail valide");}
	if(titre=="") {
		throw new Exception("entre un titre");
	}
	if(corpsMsg.contains("PJ") || corpsMsg.contains("joint") || corpsMsg.contains("jointe")) {
		if(chemins.isEmpty()) {
			throw new Exception("il n'y a pas de pièces jointe même si le corps en parle");
		}
	}
}
}
